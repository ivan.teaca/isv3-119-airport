# frozen_string_literal: true

module Api
  module V1
    class PlanesController < ApplicationController
      def index
        planes = Plane.all
        render json: planes, status: 200
      end

      def create
        plane = Plane.new(
          company: plane_params[:company],
          brand: plane_params[:brand],
          model: plane_params[:model],
          seats: plane_params[:seats]
        )
        if plane.save
          render json: plane, status: 200
        else
          render json: { error: 'Creating error' }
        end
      end

      def show
        plane = Plane.find_by(id: params[:id])
        if plane
          render json: plane, status: 200
        else
          render json: { error: 'Plane not found' }
        end
      end

      private

      def plane_params
        params.require(:plane).permit(%i[
                                        company
                                        brand
                                        model
                                        seats
                                      ])
      end
    end
  end
end
