# frozen_string_literal: true

Plane.create([
               { company: 'Air Moldova', brand: 'Airbus', model: 'A320', seats: 180 },
               { company: 'Lufthansa', brand: 'Boeing', model: '747', seats: 364 },
               { company: 'British Airways', brand: 'Boeing', model: '777', seats: 336 },
               { company: 'Emirates', brand: 'Airbus', model: 'A330', seats: 278 },
               { company: 'Fly One', brand: 'Embraer', model: 'E190', seats: 114 }
             ])
